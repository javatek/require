package org.bitbucket.javatek.require;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 *
 */
public final class ObjectRequires {
  @Nonnull
  public static <T> T requireNonNull(@Nullable T obj) {
    if (obj == null)
      throw new NullPointerException();
    return obj;
  }

  @Nonnull
  public static <T> T requireNonNull(@Nullable T obj, String error) {
    if (obj == null)
      throw new NullPointerException(error);
    return obj;
  }
}

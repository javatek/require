package org.bitbucket.javatek.require;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 *
 */
public final class StringRequires {
  @Nonnull
  public static String requireNonEmpty(@Nullable String string) {
    if (string == null || string.isEmpty())
      throw new NonEmptyStringRequiredException();
    return string;
  }

  @Nonnull
  public static String requireNonEmpty(@Nullable String string, String error) {
    if (string == null || string.isEmpty())
      throw new NonEmptyStringRequiredException(error);
    return string;
  }

  /**
   *
   */
  static class NonEmptyStringRequiredException extends IllegalArgumentException {
    NonEmptyStringRequiredException() {}

    NonEmptyStringRequiredException(String message) {
      super(message);
    }
  }
}

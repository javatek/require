package org.bitbucket.javatek.require;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.Duration;

/**
 *
 */
public final class NumberRequires {
  @Nonnull
  public static <T extends Number> T requirePositive(@Nullable T number) {
    if (number == null || number.intValue() <= 0)
      throw new PositiveNumberRequiredException(number + " is not positive!");
    return number;
  }

  @Nonnull
  public static Duration requirePositive(@Nullable Duration duration) {
    if (duration == null || duration.isNegative() || duration.isZero())
      throw new PositiveNumberRequiredException(duration + " is not positive!");
    return duration;
  }

  @Nonnull
  public static <T extends Number> T requireNonNegative(@Nullable T number) {
    if (number == null || number.intValue() < 0)
      throw new NonNegativeNumberRequiredException(number + " is negative!");
    return number;
  }

  @Nonnull
  public static Duration requireNonNegative(@Nullable Duration duration) {
    if (duration == null || duration.isNegative())
      throw new NonNegativeNumberRequiredException(duration + " is negative!");
    return duration;
  }

  /**
   *
   */
  static class PositiveNumberRequiredException extends IllegalArgumentException {
    PositiveNumberRequiredException(String message) {
      super(message);
    }
  }

  /**
   *
   */
  static class NonNegativeNumberRequiredException extends IllegalArgumentException {
    NonNegativeNumberRequiredException(String message) {
      super(message);
    }
  }
}

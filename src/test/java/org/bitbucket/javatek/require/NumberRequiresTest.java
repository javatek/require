package org.bitbucket.javatek.require;

import org.junit.Test;

import java.time.Duration;

import static org.junit.Assert.assertEquals;

/**
 *
 */
public class NumberRequiresTest {
  @Test(expected = NumberRequires.PositiveNumberRequiredException.class)
  public void requirePositiveNumber1() {
    NumberRequires.requirePositive((Number) null);
  }

  @Test(expected = NumberRequires.PositiveNumberRequiredException.class)
  public void requirePositiveNumber2() {
    NumberRequires.requirePositive(-1);
  }

  @Test(expected = NumberRequires.PositiveNumberRequiredException.class)
  public void requirePositiveNumber3() {
    NumberRequires.requirePositive(0);
  }

  @Test
  public void requirePositiveNumber4() {
    assertEquals(Integer.valueOf(12), NumberRequires.requirePositive(12));
  }

  @Test(expected = NumberRequires.NonNegativeNumberRequiredException.class)
  public void requireNonNegativeNumber1() {
    NumberRequires.requireNonNegative((Number) null);
  }

  @Test(expected = NumberRequires.NonNegativeNumberRequiredException.class)
  public void requireNonNegativeNumber2() {
    NumberRequires.requireNonNegative(-1);
  }

  @Test
  public void requireNonNegativeNumber3() {
    assertEquals(Integer.valueOf(0), NumberRequires.requireNonNegative(0));
  }

  @Test
  public void requireNonNegativeNumber4() {
    assertEquals(Integer.valueOf(10), NumberRequires.requireNonNegative(10));
  }

  @Test(expected = NumberRequires.PositiveNumberRequiredException.class)
  public void requirePositiveDuration1() {
    NumberRequires.requirePositive((Duration) null);
  }

  @Test(expected = NumberRequires.PositiveNumberRequiredException.class)
  public void requirePositiveDuration2() {
    NumberRequires.requirePositive(Duration.ZERO);
  }

  @Test
  public void requirePositiveDuration3() {
    assertEquals(Duration.ofDays(1), NumberRequires.requirePositive(Duration.ofDays(1)));
  }

  @Test(expected = NumberRequires.NonNegativeNumberRequiredException.class)
  public void requireNonNegativeDuration1() {
    NumberRequires.requireNonNegative((Duration) null);
  }

  @Test
  public void requireNonNegativeDuration2() {
    assertEquals(Duration.ZERO, NumberRequires.requireNonNegative(Duration.ZERO));
  }

  @Test
  public void requireNonNegativeDuration3() {
    assertEquals(
      Duration.ofMillis(1231),
      NumberRequires.requireNonNegative(Duration.ofMillis(1231)));
  }
}
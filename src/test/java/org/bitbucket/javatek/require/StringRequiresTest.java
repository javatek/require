package org.bitbucket.javatek.require;

import org.bitbucket.javatek.require.StringRequires.NonEmptyStringRequiredException;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 *
 */
public class StringRequiresTest {
  private static final String STRING_SHOULD_NOT_BE_EMPTY = "String should not be null or empty";
  public static final String NOT_EMPTY_STRING = "Not empty string";

  @Test(expected = NonEmptyStringRequiredException.class)
  public void requireNonEmpty1() {
    StringRequires.requireNonEmpty(null);
  }

  @Test(expected = NonEmptyStringRequiredException.class)
  public void requireNonEmpty2() {
    StringRequires.requireNonEmpty(null, STRING_SHOULD_NOT_BE_EMPTY);
  }

  @Test(expected = NonEmptyStringRequiredException.class)
  public void requireNonEmpty3() {
    StringRequires.requireNonEmpty("");
  }

  @Test(expected = NonEmptyStringRequiredException.class)
  public void requireNonEmpty4() {
    StringRequires.requireNonEmpty("", STRING_SHOULD_NOT_BE_EMPTY);
  }

  @Test
  public void requireNonEmpty5() {
    assertNotNull(StringRequires.requireNonEmpty(NOT_EMPTY_STRING));
    assertNotNull(StringRequires.requireNonEmpty(NOT_EMPTY_STRING, STRING_SHOULD_NOT_BE_EMPTY));
  }
}
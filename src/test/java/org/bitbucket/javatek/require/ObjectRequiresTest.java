package org.bitbucket.javatek.require;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 *
 */
public class ObjectRequiresTest {
  private static final Object NON_NULL_OBJ = new Object();
  private static final String OBJ_SHOULD_NOT_BE_NULL = "Object should not be null";

  @Test(expected = NullPointerException.class)
  public void requireNonNull1() {
    ObjectRequires.requireNonNull(null);
  }

  @Test(expected = NullPointerException.class)
  public void requireNonNull2() {
    ObjectRequires.requireNonNull(null, OBJ_SHOULD_NOT_BE_NULL);
  }

  @Test
  public void requireNonNull3() {
    assertNotNull(ObjectRequires.requireNonNull(NON_NULL_OBJ));
    assertNotNull(ObjectRequires.requireNonNull(NON_NULL_OBJ, OBJ_SHOULD_NOT_BE_NULL));
  }
}